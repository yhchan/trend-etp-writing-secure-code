# Validation Homework

## Installation
- 略

## Description
- reader.py 有一個嚴重的問題，可以讓使用者讀取不該讀取的檔案
- 正常的使用情境像是
    - `curl http://localhost:5000/reader/psalm.txt`
- 但是使用者可以透過 ../ 找到上層或是更多層的檔案
    - `curl http://localhost:5000/reader/../reader.py`
    - `curl http://localhost:5000/reader/../../../../../../../etc/passwd`

## Requirements
- 請修改 reader.py 以避免使用者讀取不該讀取的檔案
- 方式不限於 regular expression
