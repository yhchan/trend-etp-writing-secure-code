from flask import Flask, abort

import os
import appconfig
import re

app = Flask(__name__)
app.config.from_object(appconfig)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


@app.route("/reader/<path:filename>")
def reader(filename):
    # FIXME: Should only allow .txt in files folder
    if not re.match(r'.+', filename):
        abort(403)

    basedir = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(basedir, 'files', filename)

    if not os.path.isfile(path):
        abort(404)

    with open(path) as f:
        return f.read()


if __name__ == "__main__":
    # Be careful !!!
    app.run(host='0.0.0.0')
