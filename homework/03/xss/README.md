[Cross-site Scripting (XSS) - OWASP]: https://www.owasp.org/index.php/Cross-site_Scripting_%28XSS%29
[Testing for Cross site scripting - OWASP]: https://www.owasp.org/index.php/Testing_for_Cross_site_scripting
[XSS (Cross Site Scripting) Prevention Cheat Sheet - OWASP]: https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet

# XSS Homework


## Description
- Material 有以下幾種 XSS 漏洞
    - Reflected (Non-persistent) XSS
    - Stored (Persistent) XSS
    - DOM XSS


## Requirements
1. 你可以修復這幾種漏洞嗎？
2. 如果你是 QA 你要怎麼設計測試的機制？


## Extra Point
- 除了 escape 之外，還有什麼資源是可能防止 XSS 的
- 除了 JavaScript 跟 HTML 之外，還有哪些地方可能造成 XSS
- 為什麼 github.com 讓使用者的網站放在 github.io 而不是 github.com，像是 [PyCon 2013 Slides](http://pycon.github.io/2013-slides/)？


## Reference
- [Cross-site Scripting (XSS) - OWASP][Cross-site Scripting (XSS) - OWASP]
- [Testing for Cross site scripting - OWASP][Testing for Cross site scripting - OWASP]
- [XSS (Cross Site Scripting) Prevention Cheat Sheet - OWASP][XSS (Cross Site Scripting) Prevention Cheat Sheet - OWASP]
