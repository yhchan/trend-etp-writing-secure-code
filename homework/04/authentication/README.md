# Authentication Homework

## Description
- 原本的密碼是單純的 MD5 儲存的，知道不能只以 MD5 儲存密碼之後，管理者希望加上隨機的 salt 來確保密碼不會太容易被破解
- hubert 的原始密碼是 123456，tracy 的是 111111
- 在重新產生密碼之後，hubert 跟 tracy 還必須可以用原始的帳號密碼登入

## Requireiment
- 基本上，我們不希望取得使用者的原始密碼明碼
- 請幫助系統管理者以新的 hash algorithm 重新產生 salted 過後的 hash
- 新的 hash alrogithm 請不要使用 MD5
- 確認使用者可以用原始的帳號密碼登入
