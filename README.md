# Course Name: **Writing Secure Code**
* * *

# Objective
    - The course begins with the introduction of contemporary security issues
      and is followed by security coding techniques. Through the training of
      this course, participants will be aware of security and furthermore be
      able to delivery secure applications.

# Syllabus
    * Well-Known Insecure Code and Solution
        - Buffer Overflow
        - XSS
        - CSRF
        - SQL Injection
    * Concepts
        - Insecure Data Storage
        - Poor Authorization and Authentication
    * Secure Develop Lifecycle
        - Threat Modeling
    * Secure Configurations
    * Input Filtering

# Evaluation Criteria
    * Lab: 100%
    * Wargame: optional
    * Hacking Technique: optional

# Duration of each session
    * 8 hours

# Prerequisite
    * None

# Target Audience
    * All Trenders
