#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int foo(const char* input)
{
  char buf[10] = {0};
  strcpy(buf, input);
  printf("Now the stack looks like:\n%p\n%p\n%p\n%p\n%p\n%p\n%p\n\n");
  return 0;
}

void bar()
{
  printf("Augh! I've been hacked!");
  exit(-1);
}

int main(int argc, char* argv[])
{
  printf("Address of foo = %p\n", foo);
  printf("Address of bar = %p\n", bar);
  foo(argv[1]);

  return 0;
}
