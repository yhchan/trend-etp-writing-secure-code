#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int foo (const char* in)
{
    char buf[256];

    strncpy(buf, in, sizeof(buf));
    buf[sizeof(buf)] = 0;   //ooops - off by one!
    printf("%s\n", buf);
    return 0;
}

void bar (const char *in)
{
    printf("Augh! I've been hacked!\n");
    exit(-1);
}

int dummy (const char *in)
{
    foo(in);
    return 2;
}

int main(int argc, char* argv[]) {
  printf("Address foo = %p\n", foo);
  printf("Address bar = %p\n", bar);
  dummy(argv[1]);
  return 0;
}
