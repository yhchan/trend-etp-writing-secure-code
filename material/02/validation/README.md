[Form Validation with WTForms]: http://flask.pocoo.org/docs/patterns/wtforms/
[An Introduction to Python's Flask Framework]: http://net.tutsplus.com/tutorials/python-tutorials/an-introduction-to-pythons-flask-framework/
[Intro to Flask: Adding a Contact Page]: http://net.tutsplus.com/tutorials/python-tutorials/intro-to-flask-adding-a-contact-page/
[jquery-validation]: https://github.com/jzaefferer/jquery-validation

# Validation Examples

## Installation
- Read [An Introduction to Python's Flask Framework][An Introduction to Python's Flask Framework] first
- Use virtualenv
- pip install -r requirements.txt
- copy appconfig.py.default to appconfig.py
- Modify it for your environment
- python validation.py
- Go to [http://localhost:5001](http://localhost:5001)


## Frontend Validation
- Using jquery-validation


## Backend Validation
- Using Flask-WTF


## Reference
- [Form Validation with WTForms][Form Validation with WTForms]
- [An Introduction to Python's Flask Framework][An Introduction to Python's Flask Framework]
- [Intro to Flask: Adding a Contact Page][Intro to Flask: Adding a Contact Page]
- [jquery-validation][jquery-validation]
