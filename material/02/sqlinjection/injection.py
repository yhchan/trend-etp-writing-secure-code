from flask import Flask, _app_ctx_stack, Response, abort

import MySQLdb
import json

import appconfig

app = Flask(__name__)
app.config.from_object(appconfig)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


def init_db():
    """Creates the database tables."""
    with app.app_context():
        db = get_db()
        with app.open_resource('init.sql') as f:
            db.cursor().execute(f.read())
            db.cursor().executemany(
                'INSERT INTO client (name) VALUES (%s)',
                [('hubert'), ('bob'), ('amy'), ('tom')]
            )
        db.commit()


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    top = _app_ctx_stack.top
    if not hasattr(top, 'db'):
        db = MySQLdb.connect(host=app.config['DB_HOST'],
                             user=app.config['DB_USER'],
                             passwd=app.config['DB_PASS'],
                             db=app.config['DB_DATABASE'])
        top.db = db

    return top.db


@app.teardown_appcontext
def close_db_connection(exception):
    """Closes the database again at the end of the request."""
    top = _app_ctx_stack.top
    if hasattr(top, 'db'):
        top.db.close()


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/injection/user/<name>")
def injection(name):
    db = get_db()
    cursor = db.cursor(MySQLdb.cursors.DictCursor)

    sql = """SELECT id from client WHERE name = '%s'""" % name
    cursor.execute(sql)

    result = cursor.fetchall()
    return Response(json.dumps(result), mimetype='application/json')


@app.route("/blind_injection/user/<user_id>")
def blind_injection(user_id):
    db = get_db()
    cursor = db.cursor(MySQLdb.cursors.DictCursor)

    sql = """SELECT * from client WHERE id = '%s'""" % user_id
    cursor.execute(sql)

    result = cursor.fetchone()
    if result:
        return 'User existed'
    else:
        abort(404)


if __name__ == "__main__":
    init_db()

    # Be careful !!!
    app.run(host='0.0.0.0')
