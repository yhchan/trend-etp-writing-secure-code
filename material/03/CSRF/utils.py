import bcrypt


def get_hash(astring):
    salt = bcrypt.gensalt()
    password_hashed = bcrypt.hashpw(astring, salt)
    return password_hashed


def check_password_hashed(password, stored_hash):
    return bcrypt.hashpw(password, stored_hash) == stored_hash
