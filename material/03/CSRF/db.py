from flask import _app_ctx_stack

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import create_engine

from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import sessionmaker

from utils import get_hash

import logging
logger = logging.getLogger(__name__)

Base = declarative_base()


# DB Setting
class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)
    password = Column(String(64))

    def __init__(self, name, password):
        self.name = name
        self.password = password


class Account(Base):
    __tablename__ = 'account'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"),
                     unique=True, nullable=False)
    balance = Column(Integer, nullable=False)

    user = relationship("User", backref=backref("account", uselist=False))

    def __init__(self, user_id, balance):
        self.user_id = user_id
        self.balance = balance


def init_db(app):
    """Creates the databases"""
    with app.app_context():
        DBSession = get_db_session_maker(app)
        db_session = DBSession()
        engine = db_session.get_bind()

        Base.metadata.drop_all(engine)
        Base.metadata.create_all(engine)

        users = [('hubert', '123456'),
                 ('tracy', '111111')]

        for (user, passwd) in users:
            user_data = User(name=user,
                             password=get_hash(passwd))
            db_session.add(user_data)
            db_session.flush()

            account = Account(user_data.id, 10000)
            db_session.add(account)
            db_session.flush()

        db_session.commit()


def get_db_session_maker(app):
    top = _app_ctx_stack.top
    if not hasattr(top, 'db_session_maker'):
        engine = create_engine(app.config['DBURL'])
        top.db_session_maker = sessionmaker(bind=engine)

    return top.db_session_maker
