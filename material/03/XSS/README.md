[An Introduction to Python's Flask Framework]: http://net.tutsplus.com/tutorials/python-tutorials/an-introduction-to-pythons-flask-framework/

# XSS Examples

## Installation
- Read [An Introduction to Python's Flask Framework][An Introduction to Python's Flask Framework] first
- Use virtualenv
- pip install -r requirements.txt
- copy appconfig.py.default to appconfig.py
- Modify it for your environment
- python xss.py
- Go to [http://localhost:5001](http://localhost:5001)


## Reference
- [An Introduction to Python's Flask Framework][An Introduction to Python's Flask Framework]
