from flask import _app_ctx_stack

from sqlalchemy import Column, Integer, String, Text, DateTime, ForeignKey
from sqlalchemy import create_engine

from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import sessionmaker

from utils import get_hash

import datetime

import logging
logger = logging.getLogger(__name__)

Base = declarative_base()
Base.__table_args__ = {'mysql_charset': 'utf8'}


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)
    password = Column(String(64))

    messages = relationship("Message", backref=backref("user"))

    def __init__(self, name, password):
        self.name = name
        self.password = password


class Message(Base):
    __tablename__ = 'message'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    message = Column(Text, nullable=False)
    post_date = Column(DateTime(timezone=True), nullable=False)

    def __init__(self, user_id, message, post_date=None):
        self.user_id = user_id
        self.message = message
        if not post_date:
            post_date = datetime.datetime.now()
        self.post_date = post_date


def init_db(app):
    """Creates the databases"""
    with app.app_context():
        DBSession = get_db_session_maker(app)
        db_session = DBSession()
        engine = db_session.get_bind()

        Base.metadata.drop_all(engine)
        Base.metadata.create_all(engine)

        users = [('hubert', '123456'), ('tracy', '111111')]

        for (user, passwd) in users:
            user_data = User(name=user,
                             password=get_hash(passwd))
            db_session.add(user_data)
            db_session.flush()

            message = Message(user_data.id, 'yo: %s' % user_data.name)
            db_session.add(message)
            db_session.flush()

        db_session.commit()


def get_db_session_maker(app):
    top = _app_ctx_stack.top
    if not hasattr(top, 'db_session_maker'):
        engine = create_engine(app.config['DBURL'])
        top.db_session_maker = sessionmaker(bind=engine)

    return top.db_session_maker
