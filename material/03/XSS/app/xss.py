from flask import Flask, _app_ctx_stack, render_template, flash, \
    redirect, url_for, request, session, Response

from flask.ext.wtf import Form, TextField, PasswordField, \
    validators, SubmitField, TextAreaField

from sqlalchemy import desc, asc
from sqlalchemy.orm.exc import NoResultFound

import time
import datetime
import json

import appconfig

from db import init_db, get_db_session_maker
from db import User, Message

from utils import check_password_hashed

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


# Flask Setting
app = Flask(__name__)
app.config.from_object(appconfig)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


# Login Form
class LoginForm(Form):
    name = TextField('Name', [validators.Required()])
    password = PasswordField('Password', [validators.Required()])
    submit = SubmitField('Send')


# Message Form
class MessageForm(Form):
    message = TextAreaField('Message', [validators.Required()])
    submit = SubmitField('Send')


@app.teardown_appcontext
def close_db_connection(exception):
    """Closes the database again at the end of the request."""
    top = _app_ctx_stack.top
    if hasattr(top, 'db_session_maker'):
        top.db_session_maker.close_all()


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'POST' and form.validate():
        DBSession = get_db_session_maker(app)
        db_session = DBSession()

        try:
            user = db_session.query(User).filter(
                User.name == form.name.data,
            ).one()
        except NoResultFound:
            flash('Cannot find user')
            return redirect(url_for('login'))

        if not check_password_hashed(form.password.data, user.password):
            flash('Password not match')
            return redirect(url_for('login'))

        session['id'] = user.id
        session['name'] = user.name

        return redirect(url_for('board'))

    if 'id' not in session:
        return render_template('login.html', form=form)

    return redirect(url_for('board'))


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('login'))


@app.route('/message/read', methods=['GET', 'POST'])
def board():
    messages = _get_all_messages()
    return render_template('board.html', messages=messages)


@app.route('/message/post', methods=['GET', 'POST'])
def post_message():
    if 'id' not in session:
        flash('Login First')
        return redirect(url_for('login'))

    form = MessageForm()

    DBSession = get_db_session_maker(app)
    db_session = DBSession()

    if request.method == 'POST' and form.validate():
        message = Message(session['id'], form.message.data)
        db_session.add(message)
        db_session.commit()

        flash('Message Posted')
        return redirect(url_for('board'))

    return render_template('post.html', form=form)


@app.route('/message/search')
def search_message():
    DBSession = get_db_session_maker(app)
    db_session = DBSession()

    keyword = request.args.get('q', '')

    messages = db_session.query(Message).filter(
        Message.message.like('%%%s%%' % keyword)
    ).order_by(desc(Message.post_date)).all()

    return render_template('search.html',
                           keyword=keyword, messages=messages)


@app.route('/message/latest')
def latest_message():
    messages = _get_all_messages()
    return render_template('latest.html', messages=messages)


@app.route('/message/update/<int:timestamp>')
def message_update(timestamp):
    DBSession = get_db_session_maker(app)
    db_session = DBSession()

    date = datetime.datetime.fromtimestamp(timestamp)

    messages = db_session.query(Message).filter(
        Message.post_date > date
    ).order_by(asc(Message.post_date)).all()

    def make_resp(message):
        return {'name': message.user.name,
                'post_date': time.mktime(message.post_date.timetuple()),
                'message': message.message.encode('utf8'),
                }
    resp = json.dumps([make_resp(message) for message in messages])
    return Response(resp, mimetype='application/json')


def _get_all_messages():
    DBSession = get_db_session_maker(app)
    db_session = DBSession()

    messages = db_session.query(Message).order_by(
        desc(Message.post_date)
    ).all()
    return messages


if __name__ == "__main__":
    init_db(app)

    # Be careful !!!
    app.run(host='0.0.0.0', port=5001)
