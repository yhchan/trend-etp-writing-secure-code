[Uncover Security Design Flaws Using The STRIDE Approach]: http://msdn.microsoft.com/en-us/magazine/cc163519.aspx
[Threat Risk Modeling - OWASP]: https://www.owasp.org/index.php/Threat_Risk_Modeling
[Security Bulletin Severity Rating System - Microsoft]: http://technet.microsoft.com/en-us/security/gg309177.aspx

# Use Threat Modeling to reveal security issues

## Uncover Security Design Flaws Using The STRIDE Approach
[Uncover Security Design Flaws Using The STRIDE Approach][Uncover Security Design Flaws Using The STRIDE Approach]
[Threat Risk Modeling - OWASP][Threat Risk Modeling - OWASP]


## STRIDE
- Spoofing
- Tampering
- Repudiation
- Information disclosure
- Denial of service
- Elevation of privilege


## Steps
1. Identify Security Objectives
2. Survey the Application
3. Decompose it
4. Identify Threats
5. Identify Vulnerabilities


## Tools
### Data Flow Diagrams (DFD)
- Visualization of your data flow
    - Interactor
    - Process
    - Data Flow
    - Data Store
    - Trust Boundary
- Use STRIDE to analyze each data flow


## Mitigation
- Authentication
- Integrity
- Non-repudiation
- Confidentiality
- Availability
- Authorization
