[Plaintext]: http://en.wikipedia.org/wiki/Plaintext
[plainpass]: http://plainpass.com/
[plaintextoffenders]: http://plaintextoffenders.com/
[Best practicing for password protection]: http://blog.gcos.me/2012-01-08_best_practicing_for_password_protection.html
[Rainbow Table]: http://en.wikipedia.org/wiki/Rainbow_table
[Free Rainbow Tables]: https://www.freerainbowtables.com/
[GPU speed estimations]: http://golubev.com/gpuest.htm
[Cracking salted SHA1 password hashes on GPU]: http://gpuscience.com/cs/cracking-salted-sha1-password-hashes-on-gpu/
[CloudCracker]: https://www.cloudcracker.com/
[Password Cracking on Amazon EC2]: http://du.nham.ca/blog/posts/2013/03/08/password-cracking-on-amazon-ec2/

# Authentication Deisgn

## First Principle
- Never store plain password
- If the database/table leaked, all user passwords are leaked

### References
- [我的密碼沒加密][plainpass]
- [Plain Text Offenders][plaintextoffenders]
- [Plaintext - Wikipedia][Plaintext]

## Encrypt your password

### Strategy
- [Best practicing for password protection][Best practicing for password protection]
- Using
    - Salted Hash (with random salt)
        - Don't use SHA1/MD5 anymore
    - HMAC (with random salt)

## Attacks

### Rainbow Table Attack
 - time-storage trade-off by pre-computing hashes
 - Storage was cheaper than CPU before
 - References
    - [Rainbow Table][Rainbow Table]
    - [Free Rainbow Tables][Free Rainbow Tables]

### GPU recomputing
- [GPU speed estimations][GPU speed estimations]
    - Radeon HD 5770 (price $130): 680M SHA1 hashes/sec
- [Cracking salted SHA1 password hashes on GPU][Cracking salted SHA1 password hashes on GPU]

### Cryptography Algorithms
- Consider using strong algorithm
    - bcrypt
    - PBKDF2
    - scrypt

## Password Cracking References
- [CloudCracker][CloudCracker]
- [Password Cracking on Amazon EC2][Password Cracking on Amazon EC2]
